
# We are looking for and improving the listing in two cases:
#
# 1. The line has a "db" directive, which results in this:
# 
# 123456 02D9             screen_layout:  db 07fh,70h,03h
#             7F 70 03
# 123457 ...
#
# 2. The line has a "dw" directive, which results in this:
#
#   1443 08E5                             dw c0_answerback
#             0009
#   1444 ...
#
# In the first case, the hex bytes on the second line, up to four
# of them, will be inserted into the first line. Subsequent lines
# of four bytes don't have a line number either, but they are left
# alone, else I'd be renumbering the entire listing (which I could
# do, but I'm looking to prove that I've done the right thing with
# ease.)
#
# In the second case, the hex word is split into bytes, as that makes
# it clearer that the representation is already little-endian (i.e.
# the bytes are in the correct order in the listing.)
#
# Hex bytes in the listings are uppercase, so only check for them.
#
use v5.10;
use strict;
use Text::Tabs;

my $held_line = '';

while (my $orig = <>) {
    chomp $orig;
    my $line = expand($orig);

    # Find a line with a line number, address but no hex bytes following
    # This will be a listing line where the assembler has placed the hex
    # data on the following line, breaking up the listing. It does this
    # for "db" directives. In this case, we'll hold up the line, grab the
    # next line and insert its hex bytes into this one, then release the
    # first and delete the second (which doesn't have a line number anyway.)
    #
    if ($held_line) {
        if ($line =~ m/[0-9A-F]{4}/) { # hex word on this line
            substr($held_line, 12, 2) = substr($line, 12, 2);
            substr($held_line, 15, 2) = substr($line, 14, 2);
        }
        else { # assume sequence of up to four bytes (and trailing spaces)
            substr($held_line, 12, 11) = substr($line . " " x 11, 12, 11);
        }

        say $held_line;
        $held_line = '';
    }
    elsif ($line =~ m/\A\s*\s\d{2,4}\s[0-9A-F]{4}\s\s/) {
        $held_line = $line;
    }
    else {
        say $line;
    }
}

if ($held_line) { # very unlikely
    say $held_line;
}
